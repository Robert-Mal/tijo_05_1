package pl.edu.pwsztar.domain.dto;

public class DetailMovieDto {
    private String title;
    private String videoId;

    public DetailMovieDto(){

    }

    public String getTitle() {
      return title;
    }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getVideoId() {
      return videoId;
    }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  @Override
    public String toString() {
      return "DetailMovieDto{" +
          "title='" + title + '\'' +
          ", videoId='" + videoId + '\'' +
          '}';
    }
}
