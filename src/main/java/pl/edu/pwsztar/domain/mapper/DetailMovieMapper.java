package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.DetailMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class DetailMovieMapper {

  public DetailMovieDto mapToDetailMovie(Movie movie) {
    DetailMovieDto detailMovieDto = new DetailMovieDto();

    detailMovieDto.setTitle(movie.getTitle());
    detailMovieDto.setVideoId(movie.getVideoID());

    return detailMovieDto;
  }
}
